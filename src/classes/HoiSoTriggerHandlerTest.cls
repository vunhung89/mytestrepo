/**
* クラス名  	:	HoiSoTriggerHandlerTest
* クラス概要 	:	HoiSoTriggerHandlerTest
* @created  :	2015/04/20 ksvc Nhung Vu
* @modified :	
*/
@isTest
private class HoiSoTriggerHandlerTest {
	
	private static HoiSo__c createHoiSo(String strName, double dTyGiaUSD, double dTyGiaAUD){
		return new HoiSo__c(
							Name = strName,
							TyGiaUSD__c = dTyGiaUSD,
							TyGiaAUD__c = dTyGiaAUD
							);
	}
	
	private static ChiNhanh__c createChiNhanh(String strName, String strHoiSoId, double dVND, double dUSD, double dAUD){
		return new ChiNhanh__c(
								Name = strName,
								HoiSo__c = strHoiSoId,
								VND__c = dVND,
								USD__c = dUSD,
								AUD__c = dAUD
								);
	}

	static testMethod void testMethodOnAfterUpdate() {

		// create a new HoiSo__c
		HoiSo__c hoiSo = createHoiSo('HoiSoTest', 20000, 21000);
		// insert HoiSo__c
		insert hoiSo;

		// create a new ChiNhanh__c belong to above HoiSo__c
		ChiNhanh__c chiNhanh = createChiNhanh('ChiNhanhTest', hoiSo.Id, 4000000, 1, 1);
		// insert ChiNhanh__c
		insert chiNhanh;

		// set new values for HoiSo__c
		hoiSo.TyGiaUSD__c = 21500;
		hoiSo.TyGiaAUD__c = 22500;
		CommonValues.isHoiSoTrigger = false;

		// テスト実行
		test.startTest();
			// update new rate of HoiSo__c
			update hoiSo;
		// テスト終了
		test.stopTest();
		
		list<ChiNhanh__c> listChiNhanh = [Select 	Id, 
													Name, 
													TongTienVNDCN__c 
											From ChiNhanh__c 
											Where HoiSo__c = : hoiSo.Id];
		
		// テストした後、結果を比較
		system.assertEquals(4044000,listChiNhanh[0].TongTienVNDCN__c);
	}
}