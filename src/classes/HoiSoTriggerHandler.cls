/**
* クラス名  	:	HoiSoTriggerHandler
* クラス概要 	:	HoiSoTriggerHandler
* @created  :	2015/04/20 ksvc Nhung Vu
* @modified :	
*/
public with sharing class HoiSoTriggerHandler {
	
	/**
	* onAfterUpdate
	* after updateを実行する
	* @param mapHoiSo(trigger.new) HoiSoリスト
	* @return　なし
	* @created: 2015/04/20 ksvc Nhung Vu
	* @modified: 
	*/
	public void onAfterUpdate(map<Id, HoiSo__c> mapHoiSo){
		
		// トランザクション
		Savepoint sp = Database.setSavepoint();
		try{
			// ChiNhanhリスト
			list<ChiNhanh__c> listChiNhanh = new list<ChiNhanh__c>();
			// HoiSoオブジェクト
			HoiSo__c hoiSo = new HoiSo__c();
			// ChiNhanhデータの取得
			listChiNhanh = HoiSoTriggerDAO.getListChiNhanhByHoiSoIds(mapHoiSo.keySet());
			
			for(ChiNhanh__c chiNhanh : listChiNhanh){
				if(mapHoiSo.containsKey(chiNhanh.HoiSo__c)){
					hoiSo = mapHoiSo.get(chiNhanh.HoiSo__c);
					// calculate ChiNhanh.TongTienVNDCN__c with new hoiSo.TyGiaUSD__c and hoiSo.TyGiaAUD__c
					chiNhanh.TongTienVNDCN__c = chiNhanh.VND__c + chiNhanh.USD__c * hoiSo.TyGiaUSD__c + chiNhanh.AUD__c * hoiSo.TyGiaAUD__c;
				}
			}
			// update ChiNhanhリスト
			if(!listChiNhanh.isEmpty()){
				update listChiNhanh;
			}
		}catch(Exception ex){
			// ロールバック
			Database.rollback(sp);
			// エラー
			mapHoiSo.values().get(0).addError(ex.getMessage());
		}
	}
}